extends MarginContainer

onready var life_indicator: PackedScene = preload("res://HUD/LifeIndicator.tscn")
onready var lives_counter: HBoxContainer = $Counters/LivesCounter
onready var hi_score_label: Label = $Counters/HiScore/Value
onready var score_label: Label = $Counters/Score/Value
var SAVE_FILEPATH = 'user://save.data'
var current_score: int = 0
var hi_score: int = 0

func _ready():
	var save_data = File.new()
	if save_data.file_exists(SAVE_FILEPATH):
		save_data.open(SAVE_FILEPATH, File.READ)
		hi_score = parse_json(save_data.get_line())['hi_score']
	else:
		hi_score = 250
	save_data.close()
	score_label.text = str(current_score)
	hi_score_label.text = str(hi_score)

func set_hi_score(value: int) -> void:
	hi_score = value
	hi_score_label.text = str(hi_score)

func increase_score(value: int) -> void:
	current_score += value
	score_label.text = str(current_score)
	if current_score > hi_score:
		hi_score = current_score
		hi_score_label.text = str(hi_score)

func add_life() -> void:
	lives_counter.add_child(life_indicator.instance())

func remove_life() -> void:
	var life = lives_counter.get_child(0)
	if life:
		lives_counter.remove_child(life)

func save_game() -> void:
	var save_data = File.new()
	save_data.open(SAVE_FILEPATH, File.WRITE)
	save_data.store_line(to_json({'hi_score': hi_score}))
	save_data.close()