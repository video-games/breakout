extends Node

onready var progress_bar = $ProgressBar
onready var label = $Label
var loader: ResourceInteractiveLoader
var current_scene: Node
var min_load_time = 10 # ms
var wait_frames: int


func _ready() ->  void:
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() -1)


func _process(delta: float) -> void:
	if loader == null:
		set_process(false)
		return

	if wait_frames > 0:
		wait_frames -= 1
		return

	var t = OS.get_ticks_msec()
	while OS.get_ticks_msec() < t + min_load_time:
		var result = loader.poll()
		if result == ERR_FILE_EOF:
			var scene = loader.get_resource()
			loader = null
			_add_new_scene(scene)
			break
		elif result == OK:
			_update_progress()
		else:
			_show_error()
			loader = null
			break


func _show_error() -> void:
	pass


func _add_new_scene(scene: Resource):
	progress_bar.visible = false
	label.visible = false
	current_scene = scene.instance()
	get_node('/root').add_child(current_scene)


func _update_progress() -> void:
	var progress = float(loader.get_stage()) / loader.get_stage_count() * 100
	progress_bar.value = progress
	wait_frames += 1


func goto_scene(path: String, show_progress: bool = true) -> void:
	print('goto scene called')
	call_deferred("_goto_scene", path, show_progress)


func _goto_scene(path: String, show_progress: bool) -> void:
	wait_frames = 10
	loader = ResourceLoader.load_interactive(path)
	assert(loader)

	set_process(true)

	current_scene.queue_free()
	progress_bar.value = 0
	progress_bar.visible = show_progress
	label.visible = show_progress