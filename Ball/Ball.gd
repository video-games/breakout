extends RigidBody2D

onready var trail = $Trail
onready var trail_line = $Trail/TrailLine
onready var visibility_notifier = $VisibilityNotifier
onready var hit_sound = $HitSound
onready var destroy_sound = $DestroySound

export var max_speed = 300
export var max_trail_length = 20
var is_moving: bool = false

signal died


func _ready():
	self.connect("body_entered", self, "_on_hit")
	visibility_notifier.connect("screen_exited", self, "_on_VisibilityNotifier_screen_exited")


func _physics_process(delta: float) -> void:
	if trail_line.points.size() > max_trail_length:
		trail_line.remove_point(0)
	trail_line.add_point(position)


func _integrate_forces(state: Physics2DDirectBodyState) -> void:
	if linear_velocity.length() > max_speed:
		linear_velocity = linear_velocity.clamped(max_speed)


func _on_VisibilityNotifier_screen_exited() -> void:
	print('ball died')
	destroy_sound.play()
	destroy_sound.connect("finished", self, "_on_destroy_sound_finished")


func _on_destroy_sound_finished():
	emit_signal("died")
	queue_free()


func _on_hit(body) -> void:
	print('hit....')
	hit_sound.play()