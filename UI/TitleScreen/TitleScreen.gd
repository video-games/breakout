extends "res://UI/BaseUI/BaseUI.gd"

onready var start_button = $MarginContainer/VBoxContainer/Buttons/Start
onready var settings_button = $MarginContainer/VBoxContainer/Buttons/Settings
onready var exit_button = $MarginContainer/VBoxContainer/BottomGroup/ExitButton
onready var credits_button = $MarginContainer/VBoxContainer/BottomGroup/CreditsButton
var SCENE_DESTINATIONS = {
	'GAME': 'res://Game/Game.tscn',
	'SETTINGS': 'res://UI/Settings/Settings.tscn',
	'CREDITS': 'res://UI/Credits/Credits.tscn'
}


func _ready():
	start_button.grab_focus()
	start_button.connect("pressed", self, "_on_Start_pressed")
	settings_button.connect("pressed", self, "_on_Settings_pressed")
	credits_button.connect("pressed", self, "_on_Credits_pressed")
	exit_button.connect("pressed", self, "_on_Exit_pressed")
	start_button.connect("focus_entered", self, "_on_button_focus_entered")
	settings_button.connect("focus_entered", self, "_on_button_focus_entered")
	credits_button.connect("focus_entered", self, "_on_button_focus_entered")
	exit_button.connect("focus_entered", self, "_on_button_focus_entered")


func _on_Start_pressed() -> void:
	print('start pressed')
	set_scene_destination(SCENE_DESTINATIONS['GAME'])
	animation_player.play('exit')


func _on_Settings_pressed() -> void:
	print('settings pressed')
	set_scene_destination(SCENE_DESTINATIONS['SETTINGS'])
	set_show_loading(false)
	animation_player.play('exit')

func _on_Credits_pressed() -> void:
	set_scene_destination(SCENE_DESTINATIONS['CREDITS'])
	set_show_loading(false)
	animation_player.play('exit')

func _on_Exit_pressed() -> void:
	get_tree().quit()

func _on_button_focus_entered():
	$ButtonFocusPlayer.play()