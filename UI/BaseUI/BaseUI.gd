extends Control

onready var animation_player: AnimationPlayer = $AnimationPlayer
var _scene_destination: String setget set_scene_destination
var _show_loading: bool = true setget set_show_loading


func _ready():
	animation_player.play("enter")
	animation_player.connect("animation_finished", self, "_on_AnimationPlayer_finished")


func set_scene_destination(value: String):
	_scene_destination = value


func set_show_loading(value: bool):
	_show_loading = value


func _on_AnimationPlayer_finished(animation_name) -> void:
	match animation_name:
		"enter":
			_entered()
		"exit":
			_exited()


func _entered():
	print('entered')


func _exited():
	print('exited')
	if _scene_destination:
		Manager.goto_scene(_scene_destination, _show_loading)
