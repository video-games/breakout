extends "res://UI/BaseUI/BaseUI.gd"

onready var credits_label = $Panel/MarginContainer/CreditsLabel

var credits = """
Programmer:
	Kyle Day

Art:
	Kyle Day
	Kenney's 1-bit texture pack
	by Kenney

	https://kenney.nl/assets/bit-pack

Music:
	Ludum Dare 30 - Track 6
	by Abstraction

	VGMA Challenge - July 3rd
	by Abstraction

	VGMA Challenge - July 23rd
	by Abstraction


	http://www.abstractionmusic.com/

Thank you for checking out my first game!
"""


func _ready() -> void:
	credits_label.text = credits


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_cancel"):
		set_scene_destination("res://UI/TitleScreen/TitleScreen.tscn")
		set_show_loading(false)
		animation_player.play('exit')