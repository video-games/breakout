extends "res://UI/BaseUI/BaseUI.gd"

onready var sound_button = $MarginContainer/VBoxContainer/TopGroup/SoundButton
onready var music_button = $MarginContainer/VBoxContainer/TopGroup/MusicButton
onready var home_button = $MarginContainer/VBoxContainer/BottomGroup/HomeButton
var sound_off_select = preload("res://UI/Settings/sound_off_select.tres")
var sound_on_select = preload("res://UI/Settings/sound_on_select.tres")
var music_off_select = preload("res://UI/Settings/music_off_select.tres")
var music_on_select = preload("res://UI/Settings/music_on_select.tres")
var sound_bus_idx = AudioServer.get_bus_index("Sounds")
var is_sound_mute = AudioServer.is_bus_mute(sound_bus_idx)
var music_bus_idx = AudioServer.get_bus_index("Music")
var is_music_mute = AudioServer.is_bus_mute(music_bus_idx)

func _ready():
	sound_button.grab_focus()
	sound_button.pressed = is_sound_mute
	sound_button.texture_focused = _get_sound_button_focus()
	music_button.pressed = is_music_mute
	music_button.texture_focused = _get_music_button_focus()
	sound_button.connect("pressed", self, "_on_Sound_pressed")
	music_button.connect("pressed", self, "_on_Music_pressed")
	home_button.connect("pressed", self, "_on_Home_pressed")
	sound_button.connect("focus_entered", self, "_on_button_focus_entered")
	music_button.connect("focus_entered", self, "_on_button_focus_entered")
	home_button.connect("focus_entered", self, "_on_button_focus_entered")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		goto_title_screen()

func _on_Sound_pressed():
	is_sound_mute = !is_sound_mute
	sound_button.texture_focused = _get_sound_button_focus()
	AudioServer.set_bus_mute(sound_bus_idx, is_sound_mute)

func _on_Music_pressed():
	is_music_mute = !is_music_mute
	music_button.texture_focused = _get_music_button_focus()
	AudioServer.set_bus_mute(music_bus_idx, is_music_mute)

func _on_Home_pressed():
		goto_title_screen()

func _get_sound_button_focus():
	if is_sound_mute:
		return sound_off_select
	else:
		return sound_on_select

func _get_music_button_focus():
	if is_music_mute:
		return music_off_select
	else:
		return music_on_select

func _on_button_focus_entered():
	$ButtonFocusPlayer.play()

func goto_title_screen():
	set_scene_destination('res://UI/TitleScreen/TitleScreen.tscn')
	set_show_loading(false)
	animation_player.play('exit')