extends KinematicBody2D

onready var destroy_sound = $DestroySound
signal died


func _ready():
	pass


func hit():
	emit_signal("died")
	$Sprite.hide()
	$CollisionShape2D.disabled = true
	destroy_sound.play()
	destroy_sound.connect("finished", self, "queue_free")