extends KinematicBody2D

onready var anchor = $Anchor
onready var push_sound = $PushSound
export var impact_force = 50
export var MAX_SPEED = 200
export var ACCELERATION = 2000
var velocity = Vector2.ZERO


func _physics_process(delta: float) -> void:
	var input_vector = get_input_vector()
	if input_vector == Vector2.ZERO:
		apply_friction(ACCELERATION * delta)
	else:
		apply_movement(input_vector * ACCELERATION * delta)
	velocity = move_and_slide(velocity)


func get_input_vector() -> Vector2:
	var input_vector = Vector2.ZERO
	input_vector.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	return input_vector.normalized()


func apply_friction(amount: float):
	if velocity.length() > amount:
		velocity -= velocity.normalized() * amount
	else:
		velocity = Vector2.ZERO


func apply_movement(acceleration: Vector2):
	velocity += acceleration
	velocity = velocity.clamped(MAX_SPEED)


func push(body):
	var paddle_hit_pos = -(anchor.global_position - body.global_position).normalized().x
	var hit_vector = Vector2(paddle_hit_pos, abs(paddle_hit_pos) - 1) * impact_force
	print('Force: ' + str(hit_vector))
	push_sound.play()
	body.apply_central_impulse(hit_vector)
