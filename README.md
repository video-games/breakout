# Breakout

Oh no.  Another breakout clone. 

This is my first game created in Godot. Well, really the first game I've made and put out there to the public. I wanted a relatively simple game to become familiar with the engine.  Overall, this is just your normal Breakout clone, complete with title screen, settings, loading screen, saving/loading hi scores, multiple lives, and, of course, brick breaking. 

The source code is available on gitlab and is free and open to use as your own or, more importantly, a reference to learn a bit about Godot for yourself.  As it is, the code is not up to my standards of clean. This may or may not be fixed later if there is interest or I'm too ashamed of my bad code out there naked to the public.

## Controls

    Move Left - Left Arrow, A
    Move Right - Right Arrow, D
    Start/Action - Space
    Back/Quit - Esc

## Art

    Kenny's 1-bit pack

https://kenney.nl/assets/bit-pack

## Music

    Ludum Dare 30 - Track 6
    by Abstraction

    VGMA Challenge - July 3rd
    by Abstraction

    VGMA Challenge - July 23rd
    by Abstraction

http://www.abstractionmusic.com/