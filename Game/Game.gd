extends Node

onready var Ball = preload("res://Ball/Ball.tscn")
onready var Brick = preload("res://Brick/Brick.tscn")
onready var bricks_group = $BricksGroup
onready var paddle: KinematicBody2D = $Paddle
onready var ball_spawn_point: Node2D = $BallSpawnPoint
onready var hud: MarginContainer = $HUD
var lives = 3
var score_multiplier = 1.0
var ball: RigidBody2D


func setup_game() -> void:
	setup_ball()
	setup_bricks()


func setup_ball():
	ball = Ball.instance()
	ball.position = ball_spawn_point.position
	ball.sleeping = true
	ball.is_moving = false
	add_child(ball)
	ball.connect("body_entered", self, "_on_Ball_body_entered")
	ball.connect("died", self, "_on_Ball_died")


func setup_bricks():
	ball.position = ball_spawn_point.position
	if bricks_group.get_child_count() == 0:
		for brick_spawn_point in $BrickSpawnPoints.get_children():
			var brick = Brick.instance()
			brick.position = brick_spawn_point.position
			brick.connect("tree_exited", self, "_on_Brick_died")
			bricks_group.add_child(brick)


func start_game() -> void:
	print('starting game')
	ball.is_moving = true
	ball.apply_central_impulse(Vector2(0, 100))


func game_over() -> void:
	score_multiplier = .5
	lives -= 1
	hud.remove_life()
	if lives > 0:
		setup_ball()
	else:
		goto_title_screen()


func end_game() -> void:
	hud.save_game()
	ball.sleeping = true
	ball.is_moving = false
	$VictorySound.play()
	$VictorySound.connect("finished", self, "setup_bricks")


func goto_title_screen() -> void:
	Manager.goto_scene('res://UI/TitleScreen/TitleScreen.tscn')

func _ready():
	for num in range(lives):
		hud.add_life()
	setup_game()


func _input(event: InputEvent) -> void:
	if event.is_action("action") and not ball.is_moving:
		start_game()
	if event.is_action("ui_cancel"):
		ball.disconnect("died", self, "_on_Ball_died")
		goto_title_screen()


func _on_Ball_died() -> void:
	game_over()


func _on_Brick_died() -> void:
	var bricks_remaining = bricks_group.get_child_count()
	print(bricks_remaining)
	if bricks_remaining == 0:
		end_game()


func _on_Ball_body_entered(body) -> void:
	print(body.get_name())
	if body.get_name() == "Paddle":
		print('hit paddle')
		if score_multiplier > 1.0:
			score_multiplier = 1.0
		paddle.push(ball)
	if body.get_name().find("Brick") > -1:
		print('hit brick')
		hud.increase_score(int(10 * score_multiplier))
		body.call_deferred('hit')
	if body.get_name().find("Wall") > -1:
		print('hit wall')
		score_multiplier += 0.1
